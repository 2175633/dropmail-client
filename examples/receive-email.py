#!/usr/bin/env python3

import dropmail

mailbox = dropmail.Dropmail()

print('List of supported domains: {}\n'.format(mailbox.supported_domains))

email_default = mailbox.default_email

email_random_domain = mailbox.new_email()

email_specific_domain = mailbox.new_email('dropmail.me')
key_specific_domain = mailbox.get_key(email_specific_domain)

print('{} ({})\n'.format(email_specific_domain, key_specific_domain))

email_restore = mailbox.restore_email('nykhafjgc@emltmp.com:0b42bee6475487beca28766e2e291fa8b')

message = mailbox.next_message()

print('To (orig): {}'.format(message['to_mail_orig']))
print('To: {}'.format(message['to_mail']))
print('From: {}'.format(message['from_mail']))
print('Subject: {}'.format(message['subject']))
print('Body:\n{}'.format(message['text']))